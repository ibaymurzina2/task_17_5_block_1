﻿
#include <iostream>
using namespace std;

class Vector
{
    private:
        double x;
        double y;
        double z;
        double x1;
        double y1;

    public:
        Vector(double _x, double _y, double _z, double _x1, double _y1): 
        x(_x), y(_y), z(_z), x1(_x1), y1(_y1)
        {}

        
        void ShowVector()
        {
            cout << x << ' ' << y << ' ' << z << ' ' << x1 << ' ' << y1;
        }

       //возвращает длину вектора
       void LengthVector()
        {        
           double vector1[] = {x, y, z, x1, y1};
           cout << '\n' << "Vector Length: " << end(vector1) - begin(vector1);
          
        }
};

int main()
{
    Vector temp(1, 2, 3, 1.4, 4.3);
    temp.ShowVector();
    temp.LengthVector();
}

